module.exports = {
  type: 'postgres',
  ssl:
    process.env.NODE_ENV === 'development' &&
    process.env.DATABASE_URL !==
      'postgres://postgres:12345678@postgres:5432/sv' &&
    process.env.DATABASE_URL !== 'postgres://postgres:12345678@postgres:5432/sv'
      ? { rejectUnauthorized: false }
      : false,

  url: process.env.DATABASE_URL,

  entities: ['dist/**/**/*.entity.{js,ts}'], // typeorm loads entities from this directory
  migrations: ['dist/database/migrations/*.js'], // typeorm loads migrations from the directory

  dropSchema: process.env.NODE_ENV === 'test' ? true : false,
  migrationsRun: process.env.NODE_ENV !== 'development' ? true : false,

  cli: {
    migrationsDir: 'src/database/migrations', // typeorm creates migrations in this directory
    entitiesDir: 'dist/**/**/*.entity.{js,ts}', // typeorm creates entities in this directory
  },
};
