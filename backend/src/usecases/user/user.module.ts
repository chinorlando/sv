import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from '@/infrastructure/common/common.module';
import { AuthModule } from '../auth/auth.module';
import { UserEntity } from '@/domain/entities/user.entity';
import { UserController } from '@/infrastructure/controllers/user/user.controller';
import { UserService } from '@/infrastructure/services/user.service';
import { UploadController } from '@/infrastructure/controllers/user/upload.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity]),
    CommonModule,
    forwardRef(() => AuthModule),
  ],
  controllers: [UserController, UploadController],
  providers: [UserService],
  exports: [UserService, TypeOrmModule],
})
export class UserModule {}
