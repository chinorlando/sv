import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PermissionEntity } from '@/domain/entities/permission.entity';
import { CommonModule } from '@/infrastructure/common/common.module';
import { PermissionController } from '@/infrastructure/controllers/permission.controller';
import { PermissionService } from '@/infrastructure/services/permisssion.service';

@Module({
  imports: [TypeOrmModule.forFeature([PermissionEntity]), CommonModule],
  controllers: [PermissionController],
  providers: [PermissionService],
})
export class PermissionModule {}
