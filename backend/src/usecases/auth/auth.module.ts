import { forwardRef, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from '@/infrastructure/common/common.module';
import { UserModule } from '@/usecases/user/user.module';
import { AuthController } from '@/infrastructure/controllers/auth/autentication.controller';
import { AuthService } from '@/infrastructure/services/auth/auth.service';
import { UserRepository } from '@/domain/repositories/user.repository';
import {
  JwtStrategy,
  LocalStrategy,
} from '@/infrastructure/common/strategies/';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }), //cuando haya muchas estrategias ejectua una por defecto con defaultstrategy
    TypeOrmModule.forFeature([UserRepository]),
    CommonModule,
    forwardRef(() => UserModule),
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [JwtStrategy, PassportModule, AuthService],
})
export class AuthModule {}
