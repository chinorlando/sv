import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleEntity } from '@/domain/entities/role.entity';
import { RoleController } from '@/infrastructure/controllers/role.controller';
import { RoleService } from '@/infrastructure/services/role.service';
import { CommonModule } from '@/infrastructure/common/common.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([RoleEntity]),
    CommonModule,
  ],
  controllers: [RoleController],
  providers: [RoleService],
  exports: [RoleService],
})
export class RoleModule {}
