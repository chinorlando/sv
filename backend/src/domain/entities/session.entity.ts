import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ItemVoteEntity } from './item-vote.entity';

@Entity('sessions')
export class SessionEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fecha_hora_inicio: Date;

  @Column()
  fecha_hora_fin: Date;

  @Column()
  lugar: string;

  @OneToMany(() => ItemVoteEntity, (sesi) => sesi.sesion)
  puntos: ItemVoteEntity[];
}
