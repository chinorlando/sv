import {
  BaseEntity,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { RoleEntity } from './role.entity';

@Entity('menus')
export class MenuEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: false, unique: true })
  nombre: string;

  @Column({ type: 'varchar', nullable: false, unique: true })
  url: string;

  @Column({ type: 'varchar', nullable: false })
  orden: string;

  @Column({ type: 'varchar', nullable: true })
  icono: string;

  @Column({ type: 'varchar', nullable: true })
  usuario_modificacion: string;

  @Column({ type: 'varchar', nullable: true })
  usuario_creacion: string;

  // relacion de menu y submenu
  @ManyToOne(() => MenuEntity, (menu) => menu.submenu)
  @JoinColumn({ name: 'id_menu' })
  menu: MenuEntity;

  @OneToMany(() => MenuEntity, (submenu) => submenu.menu)
  submenu: MenuEntity[];

  // relacion de rol y menus
  @ManyToMany(() => RoleEntity)
  @JoinTable({
    name: 'menus_roles',
    joinColumn: {
      name: 'id_menu',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'id_rol',
      referencedColumnName: 'id',
    },
  })
  menus: RoleEntity[];
}
