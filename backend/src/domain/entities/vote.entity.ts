import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ItemVoteEntity } from './item-vote.entity';
import { UserEntity } from './user.entity';

@Entity('votes')
export class VoteEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int')
  validez: Validez;

  @ManyToOne(() => UserEntity, (user) => user.votos)
  @JoinColumn({ name: 'id_user' })
  usuario: UserEntity;

  @ManyToOne(() => ItemVoteEntity, (user) => user.votos)
  @JoinColumn({ name: 'id_itemvote' })
  punto: ItemVoteEntity;
}

enum Validez {
  Si,
  No,
}
