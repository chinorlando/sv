import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('convocatories')
export class ConvocatoryEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  numero: string;
  @Column()
  tipo: string;
  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  fecha: Date;
  @Column()
  fecha_aproximado: Date;
  @Column()
  lugar: string;
}
