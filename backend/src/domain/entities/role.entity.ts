import {
  BaseEntity,
  Column,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
  JoinTable,
} from 'typeorm';
import { PermissionEntity } from './permission.entity';

@Entity('roles')
export class RoleEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ type: 'integer', name: 'id' })
  id!: number;

  @Column('character varying', { name: 'nombre' })
  nombre!: string;

  // @Column({ type: 'varchar', nullable: true })
  // usuario_modificacion: string;

  // @Column({ type: 'varchar', nullable: true })
  // usuario_creacion: string;

  // relacion de rol y permisos
  @ManyToMany(() => PermissionEntity, { cascade: true })
  @JoinTable({
    name: 'roles_permisos',
    joinColumn: {
      name: 'id_rol',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'id_permiso',
      referencedColumnName: 'id',
    },
  })
  permisos: PermissionEntity[];

  // // relacin de rol y usuarios
  // @ManyToMany(() => UserEntity)
  // @JoinTable({
  //   name: 'roles_usuarios',
  //   joinColumn: {
  //     name: 'id_rol',
  //     referencedColumnName: 'id',
  //   },
  //   inverseJoinColumn: {
  //     name: 'id_user',
  //     referencedColumnName: 'id',
  //   },
  // })
  // roles: UserEntity[];
}
