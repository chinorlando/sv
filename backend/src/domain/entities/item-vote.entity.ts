import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { SessionEntity } from './session.entity';
import { VoteEntity } from './vote.entity';

@Entity('items_vote')
export class ItemVoteEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  titulo: string;

  @Column('int')
  tipo_voto: TipoVoto;

  @Column('int')
  tipo_aprovacion: TipoAprovacion;

  @ManyToOne(() => SessionEntity, (item) => item.puntos)
  @JoinColumn({ name: 'id_sesion' })
  sesion: SessionEntity;

  // realacion con puntos a votar
  @OneToMany(() => VoteEntity, (x) => x.punto)
  votos: VoteEntity[];
}

enum TipoVoto {
  UnVoto,
  DosVotos,
  TresVotos,
}

enum TipoAprovacion {
  MayoriaSimple,
  MayoriaAbsoluta,
  MayoriaCualificada,
  TresTercios,
}
