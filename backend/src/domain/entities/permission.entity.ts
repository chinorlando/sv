import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
// import { RolEntity } from './rol.entity';

@Entity('permissions')
export class PermissionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nombre: string;
}
