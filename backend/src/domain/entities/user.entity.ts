import { Exclude } from 'class-transformer';
import {
  BaseEntity,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { VoteEntity } from './vote.entity';
import { PersonEntity } from './person.entity';
import { RoleEntity } from './role.entity';

@Entity('users')
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', nullable: false, unique: true })
  username: string;

  @Column({ type: 'varchar', nullable: false })
  @Exclude()
  password: string;

  // @Column({ type: 'varchar', nullable: true })
  // usuario_modificacion: string;

  // @Column({ type: 'varchar', nullable: true })
  // usuario_creacion: string;

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, 12);
    return hash === this.password;
  }

  // relacin de rol y usuarios
  @ManyToOne(() => RoleEntity)
  @JoinColumn({ name: 'id_rol' })
  rol: RoleEntity;

  // relacion con votos
  @OneToMany(() => VoteEntity, (photo) => photo.usuario)
  votos: VoteEntity[];

  // relacion persona y usuario
  @OneToOne(() => PersonEntity)
  @JoinColumn({ name: 'id_person' })
  persona: PersonEntity;
}
