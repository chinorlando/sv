import { EntitySubscriber } from '@/infrastructure/common/context.subscriber';
import { DataSource } from 'typeorm';

// console.log(`${__dirname}/../../../**/*.entity.{js,ts}`);
export const config = {
  dbeso: {
    type: process.env.DB_TYPE || 'postgres',
    // synchronize: false,
    // logging: true,
    // host: process.env.DB_HOST || '127.0.0.11',
    // port: process.env.DB_PORT || 5432,
    // username: process.env.DB_USER || 'postgres',
    // password: process.env.DB_PASS || '12345678',
    // database: process.env.DB_NAME || 'sv',
    url: process.env.DB_URL,
    extra: {
      connectionLimit: 3,
    },
    // entities: [`${__dirname}/../../../**/*.entity.{js,ts}`],
    entities: ['dist/**/**/*.entity.{js,ts}'],
    // entities: [
    //   process.env.NODE_ENV === 'development'
    //     ? 'dist/**/**/*.entity.{js,ts}'
    //     : `${__dirname}/../../../**/*.entity.{js,ts}`,
    // ],
    subscribers: [EntitySubscriber],
    autoLoadEntities: true,
    synchronize: true,
  },
  foo: 'dev-bar',
};

// export const AppDataSource = new DataSource({
//   type: process.env.DB_TYPE || 'postgres',
//   synchronize: false,
//   logging: true,
//   host: process.env.DB_HOST || '127.0.0.11',
//   port: parseInt(process.env.DB_PORT) || 5432,
//   username: process.env.DB_USER || 'postgres',
//   password: process.env.DB_PASS || '12345678',
//   database: process.env.DB_NAME || 'sv',
//   url: process.env.DB_URL,
//   extra: {
//     connectionLimit: 3,
//   },
//   // entities: [`${__dirname}/../../../**/*.entity.{js,ts}`],
//   entities: ['dist/**/**/*.entity.{js,ts}'],
//   // entities: [
//   //   process.env.NODE_ENV === 'development'
//   //     ? 'dist/**/**/*.entity.{js,ts}'
//   //     : `${__dirname}/../../../**/*.entity.{js,ts}`,
//   // ],
//   subscribers: [EntitySubscriber],
//   autoLoadEntities: true,
//   synchronize: true,
// });

// let dataSource: DataSource;

// export const ConnectDb = async () => {
//   dataSource = await AppDataSource.initialize();
// };
