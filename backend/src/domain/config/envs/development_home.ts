import { EntitySubscriber } from '@/infrastructure/common/context.subscriber';

// console.log(`${__dirname}/../../../**/*.entity.{js,ts}`);
export const config = {
  dbeso: {
    type: process.env.DB_TYPE || 'postgres',
    synchronize: true,
    // logging: true,
    host: process.env.DB_HOST || '192.168.99.100',
    port: process.env.DB_PORT || 5432,
    username: process.env.DB_USER || 'postgres',
    password: process.env.DB_PASS || '12345678',
    database: process.env.DB_NAME || 'sv',
    // url: process.env.DB_URL,
    extra: {
      connectionLimit: 3,
    },
    entities: [`${__dirname}/../../../**/*.entity.{js,ts}`],
    subscribers: [EntitySubscriber],
    autoLoadEntities: true,
    // synchronize: true,
  },
  foo: 'dev-bar',
};
