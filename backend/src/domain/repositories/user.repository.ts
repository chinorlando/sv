import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { RegisterUserDto } from '@/domain/dtos/user/register-user.dto';
import { UserEntity } from '@/domain/entities/user.entity';
import { AuthLoginDto } from '../dtos/auth/auth-login.dto';

@Injectable()
export class UserRepository extends Repository<UserEntity> {
  constructor(private dataSource: DataSource) {
    super(UserEntity, dataSource.createEntityManager());
  }

  async signUp(authCredentialsDto: RegisterUserDto): Promise<void> {
    const { username, password } = authCredentialsDto;

    const user = new UserEntity();
    user.username = username;
    user.password = password;

    user.password = await this.hashPassword(user.password);

    // console.log(user);

    try {
      await user.save();
    } catch (error) {
      if (error.code === '23505') {
        throw new ConflictException('Username already exists.');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  async hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, process.env.SALT_SECURITY);
  }

  async validateUserPassword(authLoginDto: AuthLoginDto): Promise<string> {
    const { username, password } = authLoginDto;
    const user = await this.findOne({ where: { username } });
    // console.log(user);
    if (user && (await user.validatePassword(password))) {
      return user.username;
    } else {
      return null;
    }
  }
}
