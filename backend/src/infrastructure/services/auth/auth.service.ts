import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
// import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { RegisterUserDto } from '@/domain/dtos/user/register-user.dto';
import { UserRepository } from '@/domain/repositories/user.repository';
import { AuthLoginDto } from '@/domain/dtos/auth/auth-login.dto';
import { JwtPayload } from '@/domain/interface/jwt-payload.interface';
import { UserService } from '@/infrastructure/services/user.service';
import { compare } from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
    private userService: UserService,
  ) {}

  async signUp(authCredentialsDto: RegisterUserDto): Promise<void> {
    return this.userRepository.signUp(authCredentialsDto);
  }

  async singIn(authLoginDto: AuthLoginDto): Promise<{ accessToken }> {
    const username = await this.userRepository.validateUserPassword(
      authLoginDto,
    );
    if (!username) {
      throw new UnauthorizedException('Invalid Credentials');
    }

    const payload: JwtPayload = { username };
    const accessToken = await this.jwtService.sign(payload);
    return { accessToken };
  }

  async userId(request) {
    // const cookie = request.cookies['jwt'];
    const jwt = request.headers.authorization;
    // console.log(jwt);
    const a = jwt.replace('Bearer ', '');
    // const jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNjMyNDYzOTQ5LCJleHAiOjE2MzI1NTAzNDl9.5hHeFcG-3grp73LjiVuB3osfcfPlSHMBTPP5fBT5LbI'
    const data = await this.jwtService.verifyAsync(a);
    return data['id'];
  }

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.userService.findOne({
      where: { username },
      ralations: [],
    });

    if (user && (await compare(password, user.password))) {
      const { ...rest } = user;
      return rest;
    }

    return null;
  }

  // tutorial ruslan
  login(user) {
    const { id } = user;
    const payload = { sub: id };

    return {
      user,
      accessToken: this.jwtService.sign(payload),
    };
  }
}
