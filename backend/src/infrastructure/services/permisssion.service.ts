import { PermissionEntity } from '@/domain/entities/permission.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AbstractService } from '../common/abstract.service';
// import { Permission } from '@/domain/entities/permission.entity';

@Injectable()
export class PermissionService extends AbstractService {
  constructor(
    @InjectRepository(PermissionEntity)
    private readonly permissionRepository: Repository<PermissionEntity>,
  ) {
    super(permissionRepository);
  }

  // async all(): Promise<Permission[]>{
  //     return await this.permissionRepository.find();
  // }
}
