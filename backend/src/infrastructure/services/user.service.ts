import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { UserEntity } from '@/domain/entities/user.entity';
import { AbstractService } from '../common/abstract.service';

@Injectable()
export class UserService extends AbstractService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private dataSource: DataSource,
  ) {
    super(userRepository);
  }

  // async all(): Promise<UserEntity[]> {
  //   return await this.userRepository.find();
  // }

  // async paginate(page = 1, relations = []): Promise<any> {
  //   const { data, meta } = await super.paginate(page, relations);

  //   return {
  //     // mapeamos cuando no intercepamos y ocultar el campo password
  //     data: data.map((user) => {
  //       const { password, ...data } = user;
  //       return data;
  //     }),
  //     meta,
  //   };
  // }

  // async create(data): Promise<UserEntity> {
  //   return await this.userRepository.save(data);
  // }

  // async findOne(condition): Promise<UserEntity> {
  //   return await this.userRepository.findOne(condition);
  // }

  // async update(id: number, data): Promise<any> {
  //   return this.userRepository.update(id, data);
  // }

  // async delete(condition): Promise<any> {
  //   return await this.userRepository.delete(condition);
  // }

  // async findOneBy(condition): Promise<UserEntity> {
  //   return await this.userRepository.findOneBy({ id: condition });
  // }
}
