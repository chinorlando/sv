import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { verify } from 'jsonwebtoken';
// import { AutenticacionService } from '@app/autenticacion/infraestructure/autenticacion.service';
// import { GlobalService } from '@app/autenticacion/infraestructure/global.service';
import { UserService } from '../services/user.service';
import { ExpressRequest } from '../common/types/expressRequest.interface';
// import { jwtConstants } from '../common/constants';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  usuarioSession: string;
  constructor(private readonly userService: UserService) {}

  async use(req: ExpressRequest, _: Response, next: NextFunction) {
    // req.usuario = null;
    // GlobalService.userSession = null; //HABILITAR CUANDO NECESITEMOS HACER USUARIO_MODIFICACION Y USUARIO_CREACION

    if (!req.headers.authorization) {
      next();
      return;
    }

    const token = req.headers.authorization.split(' ')[1];
    try {
      // const decode = verify(token, jwtConstants.secret);
      const decode = verify(token, process.env.JWT_SECRET);
      const id = (decode as any).id;
      const user = await this.userService.findOne({ where: { id } });
      // GlobalService.userSession = user.username; //HABILITAR CUANDO NECESITEMOS HACER USUARIO_MODIFICACION Y USUARIO_CREACION

      req.usuario = user; //HABILITAR CUANDO NECESITEMOS HACER USUARIO_MODIFICACION Y USUARIO_CREACION
      next();
    } catch (err) {
      console.log('err', err);
      next();
    }
  }
}
