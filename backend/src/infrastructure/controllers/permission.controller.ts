import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwt-auth.guard';
import { PermissionService } from '@/infrastructure/services/permisssion.service';
import { HasPermission } from '../common/decorators/has-permision.decorator';

@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(JwtAuthGuard)
@Controller('permissions')
export class PermissionController {
  constructor(private permissionService: PermissionService) {}

  @Get()
  @HasPermission('permissions')
  async all() {
    return await this.permissionService.all();
  }

  @Post()
  async create(@Body() body): Promise<any> {
    return await this.permissionService.create({ nombre: body.nombre });
  }

  @Get(':id')
  async get(@Param('id') id: number) {
    return this.permissionService.findOne({ where: { id } });
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() body) {
    await this.permissionService.update(id, { nombre: body.nombre });
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    return this.permissionService.delete(id);
  }
}
