import {
  BadRequestException,
  Body,
  ClassSerializerInterceptor,
  ConflictException,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
// import { AuthCredentialsDto } from './dto/auth-credentials.dto';
// import { AuthLoginDto } from './dto/auth-login.dto';
import { RegisterUserDto } from '@/domain/dtos/user/register-user.dto';
import * as bcrypt from 'bcrypt';
import { UserService } from '@/infrastructure/services/user.service';
import { AuthService } from '@/infrastructure/services/auth/auth.service';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';
import { AuthGuard } from '@nestjs/passport';
// import { UserEntity } from '@/domain/entities/user.entity';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwt-auth.guard';
// import { UserEntity } from '@/domain/entities/user.entity';

@UseInterceptors(ClassSerializerInterceptor)
@Controller('auth')
export class AuthController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private jwtService: JwtService,
  ) {}

  @Post('signup')
  async register(@Body() body: RegisterUserDto) {
    if (body.password !== body.password_confirm) {
      throw new BadRequestException('Contraseña no coincide');
    }

    body.password = await this.hashPassword(body.password);

    try {
      return await this.userService.create({
        username: body.username,
        password: body.password,
      });
    } catch (error) {
      if (error.code === '23505') {
        throw new ConflictException('Username already exists.');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  async hashPassword(password: string): Promise<string> {
    const gen_salt = await bcrypt.genSalt(parseInt(process.env.SALT_SECURITY));
    return await bcrypt.hash(password, gen_salt);
  }

  @Post('signin')
  async login(
    @Body('username') username: string,
    @Body('password') password: string,
  ): Promise<{ accessToken }> {
    const user = await this.userService.findOne({ where: { username } });
    if (!user) {
      throw new NotFoundException('El usuario no existe');
    }

    if (!(await bcrypt.compare(password, user.password))) {
      throw new NotFoundException('Contraseña incorrecta');
    }

    // const payload = { user };
    const accessToken = await this.jwtService.sign({ id: user.id });
    return { accessToken };
  }

  @Post('oneuser')
  async user(@Req() request: Request) {
    // console.log(request);
    const id = await this.authService.userId(request);

    return this.userService.findOne({ where: { id } });
  }

  //con el otro codigo de dominicode
  // tutorial ruslan
  @UseGuards(AuthGuard('local'))
  @Post('login1')
  async login1(@Req() req: any) {
    // const { id } = req.user;

    // las siguientes lineas no deben estar presente porque devuelve al usuario, la respuesta debe ser Gral.
    // porque ya sabemos que existe el usuario
    // const user = await this.userService.findOne({ where: { id } });
    // if (!user) {
    //   throw new NotFoundException('El usuario no existe');
    // }

    const data = await this.authService.login(req.user);

    // const payload = { sub: id };
    // const accessToken = await this.jwtService.sign(payload);
    // return { accessToken };
    return { message: 'login exitoso', data };
  }
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  profile(@Req() req: any) {
    return req.usuario;
  }

  @UseGuards(JwtAuthGuard)
  @Get('refresh')
  refreshToken(@Req() req: any) {
    const data = this.authService.login(req.usuario);
    return { message: 'refresh exitoso', data };
  }
}
