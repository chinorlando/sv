import {
  BadRequestException,
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
// import { UserCreateDto } from './models/user-create.dto';
// import { UserUpdateDto } from './models/user-update.dto';
import { Request } from 'express';
import { UserService } from '@/infrastructure/services/user.service';
import { AuthService } from '@/infrastructure/services/auth/auth.service';
import { UserEntity } from '@/domain/entities/user.entity';
import { UpdateUserDto } from '../../../domain/dtos/user/update-user.dto';
// import { RegisterUserDto } from '@/domain/dtos/user/register-user.dto';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwt-auth.guard';
import { RegisterUserDto } from '@/domain/dtos/user/register-user.dto';
import { HasPermission } from '@/infrastructure/common/decorators/has-permision.decorator';
// import { UserEntity } from '@/domain/entities/user.entity';

@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(JwtAuthGuard)
@Controller('users')
export class UserController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Get()
  @HasPermission('users')
  async all(): Promise<UserEntity[]> {
    return await this.userService.all(['rol']);
  }

  @Post()
  async create(@Body() body: RegisterUserDto): Promise<UserEntity> {
    const password = await bcrypt.hash('123456', 12);
    const { id_rol, ...data } = body;

    return await this.userService.create({
      ...data,
      password,
      rol: { id: id_rol },
    });
  }

  @Get(':id')
  async get(@Param('id') id: number) {
    return this.userService.findOne({ where: { id }, relations: ['rol'] });
  }

  @Put('info')
  async updateInfo(@Req() request: Request, @Body() body: UpdateUserDto) {
    const id = await this.authService.userId(request);
    await this.userService.update(id, body);
    return this.userService.findOne({ where: { id } });
  }

  @Put('password')
  async updatePassword(
    @Req() request: Request,
    @Body('password') password: string,
    @Body('password_confirm') password_confirm: string,
  ) {
    if (password !== password_confirm) {
      throw new BadRequestException('Contraseña con coincide');
    }
    const id = await this.authService.userId(request);
    const hashed = await this.hashPassword(password);
    await this.userService.update(id, {
      password: hashed,
    });
    return this.userService.findOne({ where: { id } });
  }

  async hashPassword(password: string): Promise<string> {
    const gen_salt = await bcrypt.genSalt(parseInt(process.env.SALT_SECURITY));
    return await bcrypt.hash(password, gen_salt);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() body: UpdateUserDto) {
    const { id_rol, ...data } = body;
    await this.userService.update(id, {
      ...data,
      rol: { id: id_rol },
    });
    return this.userService.findOne({ where: { id } });
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    return this.userService.delete({ id });
  }

  // @Get(':id')
  // async get(@Param('id') id: number) {
  //   return this.userService.findOne({ id }, ['role']);
  // }

  // @Get()
  // async all(@Query('page') page = 1): Promise<User[]>{
  //     return await this.userService.paginate(page);
  // }

  // @Get()
  // @HasPermission('view_users')
  // @HasPermission('users')
  // async all(@Query('page') page = 1) {
  //   return await this.userService.paginate(page, ['role']);
  // }
}
