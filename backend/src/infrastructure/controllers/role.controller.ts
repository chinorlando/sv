import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
// import * as bcrypt from 'bcrypt';
import { RoleEntity } from '@/domain/entities/role.entity';
import { RoleService } from '../services/role.service';
import { RoleCreateDto } from '@/domain/dtos/role/role-create.dto';
import { RoleUpdateDto } from '@/domain/dtos/role/role-update.dto';
import { JwtAuthGuard } from '@/infrastructure/common/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('roles')
export class RoleController {
  constructor(private roleService: RoleService) {}

  @Get()
  async all(): Promise<RoleEntity[]> {
    return await this.roleService.all();
  }

  @Post()
  async create(
    @Body() body: RoleCreateDto,
    @Body('permisos') ids: number[],
  ): Promise<RoleEntity> {
    /**
     * ids.map(id => {id}) devuelve [1,2]
     * con map convertimos a [{id:1}, {id:2}]
     */
    // console.log(body, ids);

    return await this.roleService.create({
      nombre: body.nombre,
      permisos: ids.map((id) => ({ id })),
    });
  }

  @Get(':id')
  async get(@Param('id') id: number) {
    return this.roleService.findOne({
      where: { id: id },
      relations: ['permisos'],
    });
  }

  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() body: RoleUpdateDto,
    @Body('permisos') ids: number[],
  ) {
    await this.roleService.update(id, {
      nombre: body.nombre,
    });
    const role = await this.roleService.findOne({ where: { id } });
    return this.roleService.create({
      ...role,
      permisos: ids.map((id) => ({ id })),
    });
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    return this.roleService.delete({ id });
  }

  // @Post()
  // async create(@Body() body: RoleCreateDto): Promise<any> {
  //   // const password = await bcrypt.hash('123456', 12);
  //   return await this.roleService.create({
  //     nombre: body.nombre,
  //   });
  //   // console.log(body);
  // }

  // @Put(':id')
  // async update(
  //     @Param('id') id:number,
  //     @Body() body:RoleUpdateDto
  // ){
  //     await this.roleService.update(id, body);
  //     return this.roleService.findOne({id});
  // }
}
