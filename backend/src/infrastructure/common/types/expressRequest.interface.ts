import { UserEntity } from '@/domain/entities/user.entity';
import { Request } from 'express';

export interface ExpressRequest extends Request {
  usuario?: UserEntity;
}
