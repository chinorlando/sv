import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { RoleEntity } from '@/domain/entities/role.entity';
import { UserEntity } from '@/domain/entities/user.entity';
import { AuthService } from '@/infrastructure/services/auth/auth.service';
import { RoleService } from '@/infrastructure/services/role.service';
import { UserService } from '@/infrastructure/services/user.service';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private authService: AuthService,
    private userService: UserService,
    private roleService: RoleService,
  ) {}

  async canActivate(context: ExecutionContext) {
    const access = this.reflector.get<string>('access', context.getHandler());
    if (!access) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const jwte = request.headers.authorization;
    const id = await this.authService.userId(jwte);
    const user: UserEntity = await this.userService.findOne({
      where: { id },
      relations: ['rol'],
    });
    // const roles: RolEntity = await this.roleService.findOne(
    //   { id: user.role.id },
    //   ['permissions'],
    // );
    const role: RoleEntity = await this.roleService.findOne({
      where: { id: user.rol.id },
      relations: ['permisos'],
    });

    if (request.method === 'GET') {
      return role.permisos.some(
        (p) => p.nombre === `view_${access}` || p.nombre === `edit_${access}`,
      );
    }
    return role.permisos.some((p) => p.nombre === `view_${access}`);
  }
}
