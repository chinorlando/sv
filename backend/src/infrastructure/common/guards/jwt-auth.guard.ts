import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  constructor(private jwtServices: JwtService) {
    super();
  }

  // canActivate(context: ExecutionContext) {
  //   // const request = context.switchToHttp().getRequest();
  //   const request = context.switchToHttp().getRequest();
  //   try {
  //     const jwtb = request.headers.authorization;
  //     // const jwt = request.cookies['jwt'];
  //     const jwte = jwtb.replace('Bearer ', '');
  //     // console.log(jwte);
  //     return this.jwtServices.verify(jwte);
  //   } catch (error) {
  //     return false;
  //   }
  // }

  // esta funcion ejecuta cuando el metodo de arriba canActivate() esta comentado
  handleRequest(err, user, info) {
    // You can throw an exception based on either "info" or "err" arguments
    if (err || !user) {
      throw err || new UnauthorizedException('No estas autenticado');
    }
    return user;
  }
}
