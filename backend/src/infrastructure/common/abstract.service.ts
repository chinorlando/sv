import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export abstract class AbstractService {
  protected constructor(protected readonly repository: Repository<any>) {}

  async all(condition = []): Promise<any[]> {
    return await this.repository.find({ relations: condition });
  }

  async create(data): Promise<any> {
    return await this.repository.save(data);
  }

  async findOne(condition): Promise<any> {
    // return await this.repository.findOne(condition, { relations });
    return await this.repository.findOne({
      where: condition.where,
      relations: condition.relations,
    });
  }

  async update(id: number, data): Promise<any> {
    return this.repository.update(id, data);
  }

  async delete(condition): Promise<any> {
    return await this.repository.delete(condition);
  }
}
