import { UserEntity } from '@/domain/entities/user.entity';
import { AuthService } from '@/infrastructure/services/auth/auth.service';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'username', // 'username'
      passwordField: 'password', // 'passport'
    });
  }

  async validate(
    username: string,
    password: string,
  ): Promise<UserEntity | null> {
    const user = await this.authService.validateUser(username, password);
    if (!user) {
      throw new UnauthorizedException('Usuario o contraseña no coinciden');
    }
    return user;
  }
}

// import { Strategy } from 'passport-local';
// import { PassportStrategy } from '@nestjs/passport';
// import { Injectable, UnauthorizedException } from '@nestjs/common';
// import { AuthService } from '@/infrastructure/services/auth/auth.service';

// @Injectable()
// export class LocalStrategy extends PassportStrategy(Strategy) {
//   constructor(private authService: AuthService) {
//     super();
//   }

//   async validate(username: string, password: string): Promise<any> {
//     const user = await this.authService.validateUser(username, password);
//     if (!user) throw new UnauthorizedException();

//     return user;
//   }
// }
