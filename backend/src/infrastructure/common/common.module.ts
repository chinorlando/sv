import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
// import { jwtConstants } from '@/infrastructure/common/constants';

@Module({
  imports: [
    JwtModule.register({
      // secret: jwtConstants.secret,
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '1d' },
    }),
  ],
  exports: [JwtModule],
})
export class CommonModule {}
