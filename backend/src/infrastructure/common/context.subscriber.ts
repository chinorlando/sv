import { GlobalService } from '@/infrastructure/common/global.service';
import {
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  RemoveEvent,
  UpdateEvent,
} from 'typeorm';

@EventSubscriber()
export class EntitySubscriber implements EntitySubscriberInterface {
  /**
   * Called before post insertion.
   */
  beforeInsert(event: InsertEvent<any>) {
    // event.entity.usuario_creacion = GlobalService.userSession;
    // event.entity.usuario_modificacion = GlobalService.userSession;
    console.table(event);
  }

  /**
   * Called before entity update.
   */
  beforeUpdate(event: UpdateEvent<any>) {
    // event.entity.usuario_modificacion = GlobalService.userSession;
    console.table(event);
  }

  /**
   * Called before entity removal.
   */
  beforeRemove(event: RemoveEvent<any>) {
    // event.entity.usuario_modificacion = GlobalService.userSession;
    console.table(event);
  }
}
