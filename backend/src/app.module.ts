import { APP_GUARD } from '@nestjs/core';
import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { configuration } from './domain/config'; // loggerOptions
import { AuthModule } from './usecases/auth/auth.module';
import { UserModule } from './usecases/user/user.module';
import { CommonModule } from './infrastructure/common/common.module';
import { AuthMiddleware } from './infrastructure/middlewares/auth-middlewares';
import { RoleModule } from './usecases/rol/role.module';
import { PermissionModule } from './usecases/permission/permission.module';
import { PermissionGuard } from './infrastructure/common/guards/permission.guard';
import { DataSource } from 'typeorm';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    TypeOrmModule.forRootAsync({
      useFactory: async (configService: ConfigService) => ({
        ...(await configService.get('dbeso')),
      }),
      inject: [ConfigService],
    }),
    AuthModule,
    CommonModule,
    UserModule,
    RoleModule,
    PermissionModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: PermissionGuard,
    },
  ],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes({
      path: '*',
      method: RequestMethod.ALL,
    });
  }
}

// import { Module, ValidationPipe } from '@nestjs/common';
// import { ConfigModule, ConfigService } from '@nestjs/config';
// import { APP_PIPE } from '@nestjs/core'; // APP_FILTER, RouterModule
// // import { ServeStaticModule } from '@nestjs/serve-static';
// import { TypeOrmModule } from '@nestjs/typeorm';
// // import { LoggerModule } from 'nestjs-pino';

// // import { BaseModule } from './base';
// // import { CommonModule, ExceptionsFilter } from './common';
// import { configuration } from './config'; // loggerOptions
// // import { SampleModule as DebugSampleModule } from './debug';
// // import { GqlModule } from './gql';
// // import { SampleModule } from './sample';

// @Module({
//   imports: [
//     // https://getpino.io
//     // https://github.com/iamolegga/nestjs-pino
//     // LoggerModule.forRoot(loggerOptions),
//     // Configuration
//     // https://docs.nestjs.com/techniques/configuration
//     ConfigModule.forRoot({
//       isGlobal: true,
//       load: [configuration],
//     }),
//     // Database
//     // https://docs.nestjs.com/techniques/database
//     TypeOrmModule.forRootAsync({
//       useFactory: async (config: ConfigService) => ({
//         ...(await config.get('db')),
//       }),
//       inject: [ConfigService],
//     }),
//     // Static Folder
//     // https://docs.nestjs.com/recipes/serve-static
//     // https://docs.nestjs.com/techniques/mvc
//     // ServeStaticModule.forRoot({
//     //   rootPath: `${__dirname}/../public`,
//     //   renderPath: '/',
//     // }),
//     // Service Modules
//     // CommonModule, // Global
//     // BaseModule,
//     // SampleModule,
//     // GqlModule,
//     // DebugSampleModule,
//     // Module Router
//     // https://docs.nestjs.com/recipes/router-module
//     // RouterModule.register([
//     //   {
//     //     path: 'test',
//     //     module: SampleModule,
//     //   },
//     //   {
//     //     path: 'test',
//     //     module: DebugSampleModule,
//     //   },
//     // ]),
//   ],
//   providers: [
//     // Global Guard, Authentication check on all routers
//     // { provide: APP_GUARD, useClass: AuthenticatedGuard },
//     // Global Filter, Exception check
//     // { provide: APP_FILTER, useClass: ExceptionsFilter },
//     // Global Pipe, Validation check
//     // https://docs.nestjs.com/pipes#global-scoped-pipes
//     // https://docs.nestjs.com/techniques/validation
//     {
//       provide: APP_PIPE,
//       useValue: new ValidationPipe({
//         // disableErrorMessages: true,
//         transform: true, // transform object to DTO class
//         whitelist: true,
//       }),
//     },
//   ],
// })
// export class AppModule {}
