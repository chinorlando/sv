## iniciar la aplicacion
docker-compose up --build -V -d

<!-- sin construir la imagen -->
docker-compose up && docker-compose rm -fsvd

<!-- comando para iniciar de nuevo todo -->
docker-compose up --build -V && docker-compose rm -fsv

<!-- original de la web -->
Simply run docker-compose up && docker-compose rm -fsv

<!-- otra opcion -->
docker compose run be --build -V & docker compose rm -s -f

<!-- eliminar puertos -->
netstat -aon
netstat -aon | findstr :443
taskkill /pid 10720 /F

<!-- librerias -->
npm i --save @nestjs/config 
npm i --save class-validator class-transformer 
npm i --save @nestjs/typeorm typeorm pg 
npm i --save @nestjs/passport passport 
npm i --save @nestjs/jwt passport-jwt 
npm i --save -D @types/multer
npm i bcrypt
npm i -D @types/bcrypt

<!-- git flow -->
La primera vez:
git flow init   --> todo enter
git checkout develop
git pull origin develop

<!-- Para cada feature: -->
git pull origin develop
git flow feature start s<nro_sprint>_<nombre_carateristica>

<!-- Frecuentemente realizar dentro de nuestro feature: -->
git pull origin develop

git add .
git commit -am 'Descripción del commit'
git push origin feature/s<nro_sprint>_<nombre_carateristica>

Para finalizar el feature:
- Verificar que corran todas las pruebas, si todo ok:
git checkout develop
git pull origin develop
git checkout s<nro_sprint><nombre_carateristica>

git flow finish s<nro_sprint>_<nombre_carateristica>
git pull origin develop
git push origin develop

<!-- limpio -->
<!-- La primera vez: -->
git flow init
 <!-- todo enter -->
git checkout develop
git pull origin develop

<!-- En cada feature: -->
git pull origin develop
git flow feature start <nombre_carateristica>

git add <nombre_archivo>
git add .
git commit -am 'Descripción del commit'
git push origin feature/<nombre_carateristica>

<!-- Para finalizar: -->
git flow finish <nombre_carateristica>
git pull origin develop
git push origin develop


<!-- # container stop -->
docker container stop SOMENAME

<!-- # container removal -->
docker rm -f SOMENAME